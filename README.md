# DigitalOcean Kubernetes Challenge - Deploy a GitOps CI/CD implementation

### Contact info:
- Jason White
- jason@whitedev.co.uk

### Project notes:
- https://gitlab.com/jasonwhitedev/whitedev-do-kubernetes-challenge

---

End results:
- https://k8s-challenge.whitedev.co.uk
- https://k8s-challenge.whitedev.co.uk/argo-cd
- https://k8s-challenge.whitedev.co.uk/
- https://k8s-challenge.whitedev.co.uk/2048-game/

Repositories:
- https://gitlab.com/jasonwhitedev/whitedev-do-kubernetes-challenge
- https://gitlab.com/jasonwhitedev/k8s-challenge-pipeline-repo
- https://gitlab.com/jasonwhitedev/k8s-challenge-app-repo

For this challenge I used the guides provided by dzerolabs to deploy ARGO CI.

- https://medium.com/dzerolabs/installing-ambassador-argocd-and-tekton-on-kubernetes-540aacc983b9
- https://medium.com/dzerolabs/using-tekton-and-argocd-to-set-up-a-kubernetes-native-build-release-pipeline-cf4f4d9972b0

I followed the guides apart from a few small differences.

1. I used GitLab as the git repository
2. I used DigitalOcean for the FQDN
3. I used DockerHub as the container registry - later changing over to GitLab's container regisitry in a failed attempt to solve an issue

Everything went smoothly and the two medium articles got me off to a good start.  

The only change I had to make after following the first guide was to use DigitalOcean's domain name records, so after creating a small Kubernetes cluster I then waited for the associated load balancer to come up, then created a new A record to point to it - k8s-challenge.whitedev.co.uk  

I had forgotten that the load balancer would be created automatically and made one manually, which didn't work. Only a minor set back as the correct one came up by itself after about 30 minutes. I pointed the A record at the correct load balancer and it worked.  

The rest of the process was also quite easy. Once again a few small changes were needed because I used GitLab and DockerHub.  
For the second guide I needed to work out what the DockerHub registry url was so I could fill in this variable `<docker_registry_url>`  
This was harder than expected but eventually I found a working answer: `index.docker.io`. This worked and images started to be pushed to my account on DockerHub.  

I also needed to find a value for the `git-app-repo-url` that would work when using GitLab. With some searching and testing I found that `body.project.git_http_url` was good.  
In the same block it was also necessary to check the value of `name: git-app-repo-revision`. `master` is often called `main` and it certainly is in my example, so I set this to `main`.

Then in general everything started to work. I had to simplyfy step 5 of the second guide which is for deploying the game app to a different Kubernetes cluster. For this example I altered the command to deploy the game in the same cluster. 

## Issues

Unfortunately I encountered a major issue with this process that I am unable to fix. When pushing changes to the app repository, ARGO is supposed to detect this change and then run the pipeline to deploy the new changes to the cluster automatically. This works up to a point but fails near the end of the process.  

I can see an error: `level=fatal msg="1 resources require pruning"` but this could be a red-herring. Either way there is something that is preventing the new images from being deployed.
The images are sucessfully built and sent into the container registry but then the process hits an error meaning that the updated app is not deployed.  

I managed to force the deployment by changing the version number of the app and manually recreating the pipeline but this goes against the idea of automatic deployments which is a big part of this what this process promises.  

## Conclusion

All in a all a fun project to undertake. Most parts were relatively easy with just some minor issues caused by using different resources - GitLab instead of GitHub for example.  
ARGO CI looks useful but seems to be no major improvement over GitLab CI - which I have successfully used to create CI pipelines for apps to be deployed on Kubernetes clusters. The advantage of ARGO appears to be that it helps to manage the deployment process including usful features such as rollbacks and the ability to deploy to different clusters.  

I am still hugely frustrated by the major issue described above. It is annoying that I can't get any better information from the logs to point me towards what the underlying issue actually is. I feel as if I have reached the stage of the debugging process where you start trying odd things in the hope that the issue will magically resolve itself. Perhaps moving everything onto GitHub will help? Probably not, but it is something to try at least. I have reached out to the author of the guides but they have not responded which is not surprising. Solving other peoples' akward issues is no fun. I think I will just lay this one to rest and move on. I am unlikely to need to use ARGO in the near future.